from abc import ABC, abstractmethod

class AbstractPlugin(ABC):
    def __init__(self, config={}):
      self.config = config

    @abstractmethod
    def add_context(self, message_file):
      pass
