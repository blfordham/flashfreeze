import GetOldTweets3 as got

from flashfreeze.plugins import AbstractPlugin

class Twitter(AbstractPlugin):
  def add_context(self, message_file):
    username = self.config['username']
    maxTweets = self.config['count']
    criteria = got.manager.TweetCriteria().setUsername(username).setMaxTweets(maxTweets)
    tweets = got.manager.TweetManager.getTweets(criteria)

    message_parts = []
    for tweet in tweets:
      message_parts.append("%s\n%s\n%s\n" % (tweet.text, tweet.formatted_date, tweet.permalink))
    
    message_file.write(str.encode('\n'.join(message_parts)))
    