import requests

from flashfreeze.plugins import AbstractPlugin

class OpenWeatherMap(AbstractPlugin):
  def add_context(self, message_file):
    weather_data = self.get_weather()

    unit = 'F' if self.config['units'] else 'C'

    main = weather_data['main']
    temp = int(main['temp'])
    feels_like = int(main['feels_like'])

    location = "%s, %s" % (weather_data['name'], weather_data['sys']['country'])
    description = weather_data['weather'][0]['description']

    message = "%i%s in %s (feels like %i), %s\n" % (temp, unit, location, feels_like, description)
    message_file.write(str.encode(message))

  def get_weather(self):
    url = self.build_url()
    return requests.get(url).json()

  def build_url(self):
    return 'https://api.openweathermap.org/data/2.5/weather?' + \
      'id=' + str(self.config['city']) + \
      '&units=' + self.config['units'] + \
      '&appid=' + self.config['appid']