import time

from flashfreeze.plugins import AbstractPlugin

class Timestamp(AbstractPlugin):
  def add_context(self, message_file):
    message_file.write(str.encode(time.strftime('%c %z') + '\n'))