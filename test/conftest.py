import tempfile
import pytest
from shutil import copyfile
from git import Repo
import os
import sys
import json

testdir = os.path.dirname(__file__)
sys.path.append(os.path.join(os.path.dirname(testdir), 'src'))
resource_path = os.path.join(testdir, 'resources', 'files')

def empty_repo():
  path = tempfile.mkdtemp()
  Repo.init(path)
  return path

@pytest.fixture
def create_empty_git():
  return empty_repo()

@pytest.fixture  
def add_single_file(create_empty_git):
  path = empty_repo()
  src = os.path.join(resource_path, '1.txt')
  dst = os.path.join(path, '1.txt')
  copyfile(src, dst)
  return path

@pytest.fixture
def owm_response():
  with open(os.path.join(resource_path, 'owm_response.json')) as fp:
    response = json.load(fp)
  return response

