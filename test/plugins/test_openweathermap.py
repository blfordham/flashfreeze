from flashfreeze.plugins.openweathermap import OpenWeatherMap
import pytest
import requests
from io import BytesIO

def get_config():
    config = {
      'city': 12345,
      'units': 'imperial',
      'appid': 'moocow'
    }
    return config

@pytest.mark.usefixtures("owm_response")
class TestOpenWeatherMap:
  def test_url(self):
    owm = OpenWeatherMap(get_config())
    assert owm.build_url() == 'https://api.openweathermap.org/data/2.5/weather?id=12345&units=imperial&appid=moocow'

  def test_context(self, monkeypatch, owm_response):
    class MockResponse:
      # mock json() method always returns a specific testing dictionary
      @staticmethod
      def json():
        return owm_response
    def mock_get(*args, **kwargs):
        return MockResponse()

    monkeypatch.setattr(requests, "get", mock_get)

    owm = OpenWeatherMap(get_config())
    output = BytesIO()

    owm.add_context(output)
    assert output.getvalue() == b'48F in Richmond Hill, US (feels like 43), scattered clouds\n'