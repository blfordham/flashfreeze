from flashfreeze.gitclient import GitClient
import pytest

@pytest.mark.usefixtures("create_empty_git", "add_single_file")
class TestGit:
    def test_init(self, create_empty_git):
      git = GitClient(create_empty_git)
      assert git.changed_files == []

    def test_single_file(self, add_single_file):
      git = GitClient(add_single_file)
      assert git.changed_files == ['1.txt']

    def test_commit(self, add_single_file):
      git = GitClient(add_single_file)
      message = 'Test commit'
      git.add_untracked_files()
      git.commit(message)
      assert git.untracked_files == []
      headcommit = git.head_commit()
      assert len(headcommit.hexsha) == 40
      assert len(headcommit.parents) == 0
      assert headcommit.tree.type == 'tree'
      assert len(headcommit.author.name) != 0
      assert isinstance(headcommit.authored_date, int)
      assert len(headcommit.committer.name) != 0
      assert isinstance(headcommit.committed_date, int)
      assert headcommit.message == message