import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
     name='flashfreeze',  
     version='0.1',
     scripts=['flashfreeze'] ,
     author="Bryan Fordham",
     author_email="bryan@nativesavannah.com",
     description="A modern rewrite of FlashBack",
     long_description=long_description,
   long_description_content_type="text/markdown",
     url="https://gitlab.com/blfordham/flashfreeze",
     packages=['flashfreeze'],
     package_dir={'flashfreeze': 'src/flashfreeze'},
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
 )