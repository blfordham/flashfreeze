# Flashfreeze

A modern rewrite of flashbake

This is *very* rough, but it works.

You can now install via pip:

`pip install flashfreeze`


Copy `config.yaml.sample` to `~/.flashfreeze/config.yaml` and edit to your liking.

To run:
`flashfreeze`

From the python interpreter:

```python
>>> from flashfreeze import FlashFreeze
>>> f = FlashFreeze()
>>> f.run()
```

This isn't even alpha, folks. Use at your own risk. But if you have issues/bugs/suggestions, please let me know.
